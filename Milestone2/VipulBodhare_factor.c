#include <stdio.h>
 
void Find_Factors(int);  
 
int main()
{
  int Number; 
   
  printf("\n Please Enter number to Find Factors\n");
  scanf("%d", &Number);
 
  printf("\n Factors of a Number are:\n");
  Find_Factors(Number); 
 
  return 0;
}
 
void Find_Factors(int Number)
{ 
  int i; 
  
  for (i = 1; i <= Number; i++)
   {
    if(Number%i == 0)
     {
       printf("%d ", i);
     } 
   }
}