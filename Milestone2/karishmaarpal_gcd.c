#include <stdio.h>
 
int gcd(int, int);
 
int main()
{
    int x, y, result;
 
    printf("Enter the two numbers");
    scanf("%d%d", &x, &y);
    result = gcd(x, y);
    printf("The GCD of %d and %d is %d.\n", x, y, result);
}
 
int gcd(int x, int y)
{
    while (x != y)
    {
        if (x > y)
        {
            return gcd(x - y, y);
        }
        else
        {
            return gcd(x, y - x);
        }
    }
    return x;
}