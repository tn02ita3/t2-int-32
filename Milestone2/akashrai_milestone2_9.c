#include<stdio.h>
double gcd(int a,int b)
{
	if(b == 0)	
		return a;
	else
		return gcd(b,a%b);
}
void main()
{
	double d;
	int a,b,c;
	printf("Enter enter 3 numbers to find their LCM\n");
	scanf("%d%d%d",&a,&b,&c);
	if(c > a)
	{
		d = c;
		c = a;
		a = d;
	}
	if(b > a)
	{
		d = b;
		b = a;
		a = d;
	}
	if(c > b)
	{
		d = b;
		b = c;
		c = d;
	}
	d = (a*b*c)/(gcd(a,b)*gcd(a,c));
	printf("The LCM of %d,%d,%d is %Lf",a,b,c,d);
}