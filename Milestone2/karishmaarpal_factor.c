#include <stdio.h>
 
void Find_Factors(int);  
 
int main()
{
  int Number; 
   
  printf("Enter number to Find Factors");
  scanf("%d", &Number);
 
  printf("Factors of a Number are:");
  Find_Factors(Number); 
 
  return 0;
}
 
void Find_Factors(int Number)
{ 
  int i; 
  
  for (i = 1; i <= Number; i++)
   {
    if(Number%i == 0)
     {
       printf("%d ", i);
     } 
   }
}