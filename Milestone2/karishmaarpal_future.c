#include<stdio.h>

double FutureVal(double rate, unsigned int nperiods,double PV);

int main()
{

    double PV;
    double rate;
    int  nperiods;
    double result;




    printf("Present value  : ");
    scanf("%lf",&PV);

    printf(" Rate: ");
    scanf("%lf",&rate);

    printf("Periods: ");
    scanf("%d",&nperiods);

    result = FutureVal(rate,nperiods,PV);

    printf("The future value is %lf \n",result);

    return 0;

}

double FutureVal(double rate, unsigned int nperiods,double PV)
{
    int i;
    double value = 1 + rate ;
    double result = 1;

    for (i = 0; i < nperiods; i++)
    {
            result *= value;
    }
    result *= PV;
    return result;
}