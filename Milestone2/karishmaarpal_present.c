#include<stdio.h>

double PresentVal(double rate, unsigned int nperiods,double FV);

int main()
{

    double FV;
    double rate;
    int  nperiods;
    double result;




    printf("Future value  : ");
    scanf("%lf",&FV);

    printf(" Rate: ");
    scanf("%lf",&rate);

    printf("Periods: ");
    scanf("%d",&nperiods);

    result = PresentVal(rate,nperiods,FV);

    printf("The Present value is %lf \n",result);

    return 0;

}

double PresentVal(double rate, unsigned int nperiods,double FV)
{
    int i;
    double value = 1 + rate ;
    double result = 1;

    for (i = 0; i < nperiods; i++)
    {
            result *= value;
    }
    result = FV/result;
    return result;
}