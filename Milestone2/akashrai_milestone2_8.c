#include <stdio.h>
int isEven(int number)
{
    return !(number & 1);
}
int main()
{
int number;
printf("Enter any number: ");
scanf("%d", &number);
if(isEven(number))
{
printf("The number entered is even.");
}
else
{
printf("The number entered is odd.");
}
return 0;
}