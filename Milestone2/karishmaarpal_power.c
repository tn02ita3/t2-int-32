#include <stdio.h>
#include <conio.h>
  
int main(){
	int n, counter, result = 1;
    double x;
    printf("Enter base and exponent \n");
    scanf("%lf %d", &x, &n);
    
    result = getPower(x, n);
    
    printf("%lf^%d = %d", x, n, result);
    
    return 0;
}

int getPower(double x, int n){
    
    if(n == 0){
        return 1;
    }
    return x * getPower(x, n - 1);
}