#include <stdio.h>
 

void printFibonacciNumbers(int n)
{
    int f1 = 0, f2 = 1, i;
 
    if (n < 1)
        return;
 
    for (i = 1; i <= n; i++)
    {
        printf("%d ", f2);
        int next = f1 + f2;
        f1 = f2;
        f2 = next;
    }
}
 
int main()
{
	int n;
	printf("Enter the value of n ");
   scanf("%d",&n);
    printFibonacciNumbers(n);
    return 0;
}