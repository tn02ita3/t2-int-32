#include <stdio.h>
 
void Prime_Factors(int);  
 
int main()
{
  int Number; 
   
  printf("\n Please Enter number to Find its Prime Factors\n");
  scanf("%d", &Number);
 
  printf("\n Prime Factors of %d are:\n",Number);
  Prime_Factors(Number); 
 
  return 0;
}
 
void Prime_Factors(int Number)
{ 
  int i,j,count=0; 
  
  for (i = 1; i <= Number; i++)
   {
    if(Number%i == 0)
     {
       
        for(j=1;j<=(i/2);j++){
        if(i%j==0){
            count++;
        }
    }

    if(count==1){
        printf("%d ", i);}
        count=0;
     } 
   }
}