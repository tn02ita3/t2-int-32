#include <stdio.h>
int main()
{
  int arr[10], search, i, n, flag=0;
  printf("Enter total number of elements for the array(max 10)\n");
  scanf("%d",&n);
  printf("Enter elements\n",n);
  for (i=0;i<n;i++)
    scanf("%d",&arr[i]);
  printf("\nEnter the number to search from array list \n");
  scanf("%d", &search);
  for (i=0;i<n;i++)
  {
    if (arr[i]==search)
    {
      printf("%d is at index %d.\n", search, i+1);
      flag=1;
      break;
    }
  }
  if (flag==0)
    printf("%d is not present in the list of elements \n", search);
  return 0;
}
