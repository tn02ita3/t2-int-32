#include <stdio.h>              
#include <conio.h>

void ascending(int a[100], int n);
void descending(int a[100], int n);

int main()                       
{
	int a[100],n,i;
	char choice;
	printf("Array size: ");
    scanf("%d",&n);
    printf("Enter Elements: ");
    
	
	//for ascending order    
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
	printf("Click A for Ascending and B for Descending");
	scanf(" %c",&choice);
	
	switch(choice) {
		
		case 'A' :
		case 'a' : 
			ascending(a,n);
			break;
		case 'B' :
		case 'b' : descending(a,n);
			break;
			
		default : printf("hello.");
		
	}
	
	return 0;                                       
}

void ascending(int a[100], int n) {
	int tmp;
	for (int i = 0; i < n; i++)                     
	{
		for (int j = 0; j < n; j++)             
		{
			if (a[j] > a[i])               
			{
			 tmp = a[i];         
				a[i] = a[j];            
				a[j] = tmp;             
			}  
		}
	}
	printf("\n\nElements in Ascending order: "); 
	
	for (int i = 0; i < n; i++)                     
	{
		printf(" %d ", a[i]);                   
	}
	

}

void descending(int a[100], int n) {
	
	int tmp;
	
	
	for (int i = 0; i < n; i++)                     
	{
		for (int j = 0; j < n; j++)             
		{
			if (a[j] < a[i])                
			{
			 tmp = a[i];         
				a[i] = a[j];            
				a[j] = tmp;             
			}
		}
	}
	printf("\n\nElements in Descending order : ");   

	for (int i = 0; i < n; i++)                     
	{
		printf(" %d ", a[i]);                   
	}
	
	
	
}
