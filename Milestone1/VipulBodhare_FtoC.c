#include <stdio.h>
int main()
{
    float C, F;
    printf("Enter the temperature in Fahrenheit: ");
    scanf("%f", &F);
    C = (F - 32) * 5 / 9;
    printf("%0.2f Fahrenheit = %.2f Celsius", F, C);
    return 0;
}