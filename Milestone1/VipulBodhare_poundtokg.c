#include <stdio.h>
int main()
{
	float pound;
	printf("Enter weight in pounds: ");
	scanf("%f", &pound);
	printf("%0.2f pound = %0.2f kilograms", pound, pound*0.453592);
	return 0;
}