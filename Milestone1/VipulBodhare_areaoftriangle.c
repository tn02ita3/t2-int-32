#include <stdio.h>
int main()
{
    float b, h;
    printf("Enter the base and height of triangle: ");
    scanf("%f %f", &b, &h);
    printf("Area = %0.2f", 0.5*b*h);
    return 0;
}