#include <stdio.h>
int main()
{ 
	float a, b;
	printf("Enter radius of the circle: ");
	scanf("%f", &a);
	b = 3.14*a*a;
	printf("Area of the circle = %0.4f", b);
	return 0;
}