#include <stdio.h>

int main()
{
    float inch;
    printf("Enter inches: ");
    scanf("%f", &inch);
    printf("%0.2f inch = %0.2f cm", inch, inch*2.5);
    return 0;
}