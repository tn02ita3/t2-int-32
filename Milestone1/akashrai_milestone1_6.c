#include <stdio.h>
#include <math.h>
void main()
{
    int s, x, y, z, area;
    printf("Enter the values of x, y and z \n");
    scanf("%d %d %d", &x, &y, &z);
    s = (x + y + z) / 2;
    area = sqrt(s * (s - x) * (s - y) * (s - z));
    printf("Area of the Triangle = %d \n", area);
}