#include<stdio.h>
int str_length(char *str);
void main()
{
	char str[100];
	int length;
	printf("Enter a string: ");
	scanf("%s",str);
	length=str_length(str);
	printf("The length of the given string %s is : %d", str, length);
}
int str_length(char *str)
{
	int i=0;
	while(*str != '\0')
	{
		i++;
		*str++;
	}
	return i;
}