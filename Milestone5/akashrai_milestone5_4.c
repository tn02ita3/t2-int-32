#include<stdio.h>
int strfindchar(char *str, char *ch)
{
	int i =0;
	while(str[i++] != '\0')
	{
		if(str[i] == *ch)
		{
			return i + 1;
		}
	}
	return -1;
}
int main()
{
	char arr1[] = "Hi Akash!",p;
	printf("Enter character you want to find.\n");
	scanf("%c",&p);
	
	printf("String find function result is %d",strfindchar(arr1,&p));
	return 0;
}